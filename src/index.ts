import AuthenticatorService, {DisclosureResponseInterface} from './authenticator-service';

const registerAuthenticator: () => void = () => {
    const targetUrl = 'http://127.0.0.1:8081/accounts/verify/';

// You must have registered socket.io on window.socket to run this script
    // @ts-ignore
    if ('undefined' === typeof window.socket) {
        throw new Error('You must register socket.io under window.socket to run this script');
    }

    // @ts-ignore
    const authenticator = new AuthenticatorService(window.socket, targetUrl);
    authenticator.fetchDisclosureChallenge();

    // @ts-ignore
    window.authenticator = authenticator;
};

window.onload = () => {
    registerAuthenticator();
};