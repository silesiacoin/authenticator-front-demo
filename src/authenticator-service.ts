export enum AuthenticatorSocketMethods {
    createDisclosureRequest = 'createDisclosureRequest',
    disclosureRequestPayload = 'disclosureRequestPayload',
    disclosureRequestError = 'disclosureRequestError',
    disclosureAcceptance = 'disclosureAcceptance'
}

export interface DisclosureResponseInterface {
    disclosureRequest: string,
    qrCode: string,
    params: {
        requested: Array<string>,
        callBackUrl: string
    }
}

export interface SocketErrorInterface {
    error: string
}

export interface DisclosureAcceptance {
    profile: {
        did: string
    }
}

export default class AuthenticatorService {
    private socket: SocketExtension;

    constructor(socket: any) {
        this.socket = new SocketExtension(socket);
        this.attachListeners();
    }

    public fetchDisclosureChallenge(): void {
        this.socket.emit(AuthenticatorSocketMethods.createDisclosureRequest);
        console.log(`[SOCKET]: ${AuthenticatorSocketMethods.createDisclosureRequest}`);
    }

    private static pushIframeListener(disclosureResponse: DisclosureResponseInterface): void {
        const iframe: HTMLIFrameElement = document.createElement('iframe');
        iframe.src = disclosureResponse.qrCode;
        iframe.classList.add('body__qr');
        const bodyContainers = document.getElementsByTagName('body');

        if (bodyContainers.length !== 1) {
            throw new Error('Invalid document');
        }

        console.log('I am pushing QR code');
        bodyContainers[0].appendChild(iframe);
    }

    private static pushAuthenticatedData(disclosureAcceptance: DisclosureAcceptance): void {
        const bodyContainers = document.getElementsByTagName('body');

        if (bodyContainers.length < 1) {
            console.log('[SOCKET] Error: no body found');
        }

        const authenticationProfileDiv: HTMLDivElement = document.createElement('div');
        authenticationProfileDiv.innerHTML = `<h2>Authentication went smoothly</h2><h3>Profile: ${disclosureAcceptance.profile.did}</h3>`;
        bodyContainers[0].innerHTML = '';
        bodyContainers[0].appendChild(authenticationProfileDiv);
        console.log('[SOCKET] Authentication success.');
    }

    // In this function you handle routing with authenticator service
    private attachListeners(): void
    {
        this.socket.on(AuthenticatorSocketMethods.disclosureRequestPayload, (payload: string) => {
            const disclosureResponse: DisclosureResponseInterface = <DisclosureResponseInterface> JSON.parse(payload);
            this.socket.setState(disclosureResponse);
            console.log(`[SOCKET]: ${AuthenticatorSocketMethods.disclosureRequestPayload}: `, disclosureResponse);

            // Push iframe listener
            AuthenticatorService.pushIframeListener(disclosureResponse);
        });

        this.socket.on(AuthenticatorSocketMethods.disclosureRequestError, (payload: string) => {
            const socketError: SocketErrorInterface = <SocketErrorInterface> JSON.parse(payload);
            this.socket.setState(socketError);
            console.log(`[SOCKET]: ${AuthenticatorSocketMethods.disclosureRequestError} `, socketError);
        });

        this.socket.on(AuthenticatorSocketMethods.disclosureAcceptance, (payload: string) => {
           const disclosureAcceptance: DisclosureAcceptance = <DisclosureAcceptance> JSON.parse(payload);
           this.socket.setState(disclosureAcceptance);
           console.log(`[SOCKET] ${AuthenticatorSocketMethods.disclosureAcceptance} `, disclosureAcceptance);
           AuthenticatorService.pushAuthenticatedData(disclosureAcceptance);
        });
    }
}

interface BasicSocketInterface {
    on: (event: string, callback: (...args: any) => void) => void,
    emit: (event: string, ...args: Array<string>) => void
}

/**
 * Own socket extension to get easily get state from the socket
 */
class SocketExtension implements BasicSocketInterface {
    constructor(
        private socket: any,
        protected state: string = '',
    ) {
        if ('function' !== typeof socket.emit || 'function' !== typeof socket.on) {
            throw new Error('Invalid socket type. Must by type of socket.io');
        }
    }

    public on(event: string, callback: (...args: any) => void): void {
        this.socket.on(event, callback);
    }

    public emit(event: string, ...args: Array<string>): void {
        this.socket.emit(event, ...args);
    }

    public setState(state: any): void {
        this.state = JSON.stringify(state);
    }
}
