const path = require('path');

module.exports = {
    entry: './src/index.ts',
    resolve: {
        extensions: ['.ts']
    },
    mode: "production",
    node: {
        fs: 'empty'
    },
    output: {
        path: path.resolve(__dirname, 'src'),
        filename: './../app.js'
    },
    module: {
        rules: [
            {
                test: /\.ts$/,
                exclude: /(node_modules)/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['@babel/preset-typescript']
                    }
                }
            }
        ]
    }
};