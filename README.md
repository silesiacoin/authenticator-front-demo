# authenticator-front-demo

Demo for uPort implementation

# Setup
copy `docker-compose.override.yml.dist` to `docker-compose.override.yml`
`docker-compose up -d`

# Build manually
`docker-compose run builder npx tsc`